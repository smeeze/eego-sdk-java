# eego-SDK java wrapper

java wrapper for the eego-SDK. The wrapper tries to follow the eego-SDK API as
much as possible. Please refer to the official eego-SDK docs how to use the
API.

## requirements
* C++ compiler that supports C++11
* cmake 3.11
* git
* java
* eego-SDK

## supported platforms
* windows(32 and 64 bit)
* linux(32 and 64 bit)

## setup
create a user.cmake file in the root of this project. Add a line pointing
to the eego-SDK zip file, like so:
```
set(EEGO_SDK_ZIP /path/to/download/eego-sdk-1.3.19.40453.zip)
```

## build
build using standard cmake. i.e. create build directory and
from there, call cmake:
```
cmake -DCMAKE_BUILD_TYPE=Release /path/to/eego_gui && cmake --build .
```

## running
make sure java can find the created libraries and .jar files and the eego-SDK
library, i.e.
take a look at the Stream.java test on how to interact with the factory,
amplifier and stream objects.

The provided Stream.java test program loops over each amplifier, shows the
impedances, then shows 10 second of EEG.
