package test;

import java.lang.Math;
import java.util.TimerTask;

public class StreamPoll extends TimerTask {
    public StreamPoll(com.eemagine.sdk.stream stream, boolean is_eeg) {
        _stream = stream;
        _is_eeg = is_eeg;
    }

    @Override
    public void run() {
        try {
            var buffer = _stream.getData();
            System.out.printf("buffer: %s\n", buffer);
            for (int s = 0; s < Math.min(4, buffer.getSampleCount()); ++s) {
                System.out.printf("     :");
                for (int c = 0; c < Math.min(10, buffer.getChannelCount()); ++c) {
                    System.out.printf(" % 10.4f", buffer.getSample(c, s));
                }
                System.out.printf("\n");
            }
            if(_is_eeg) {
                for (int s = 0; s < buffer.getSampleCount(); ++s) {
                    double trigger_code = buffer.getSample(buffer.getChannelCount() - 2, s);
                    if (trigger_code > 0.0) {
                        System.out.printf("trigger %d at buffer index %d\n", (int)trigger_code, s);
                    }

                    double sample_counter = buffer.getSample(buffer.getChannelCount() - 1, s);
                    double delta = sample_counter - _sample_counter;
                    _sample_counter = sample_counter;
                    if (delta != 1.0) {
                        System.out.printf("delta %d at buffer index %d\n", (int)delta, s);
                    }
                }
            }
        } catch (Exception e) {
            System.out.printf("error getting data: %s\n", e);
        }
    }

    private com.eemagine.sdk.stream _stream;
    private double _sample_counter;
    private boolean _is_eeg;
}
