package test;

import java.util.Arrays;
import java.util.Timer;

public class Stream {
    public static void main(String[] args) {
        var factory = new com.eemagine.sdk.factory();
        System.out.printf("sdk version. %s\n", factory.getVersion());

        for (var amplifier : factory.getAmplifiers()) {
            try {
                System.out.printf("amplifier: %s-%d-%s\n", amplifier.getType(), amplifier.getFirmwareVersion(),
                        amplifier.getSerialNumber());
                System.out.printf("  channels.... %s\n", Arrays.toString(amplifier.getChannelList()));
                System.out.printf("  rates....... %s\n", Arrays.toString(amplifier.getSamplingRatesAvailable()));
                System.out.printf("  ref ranges.. %s\n", Arrays.toString(amplifier.getReferenceRangesAvailable()));
                System.out.printf("  bip ranges.. %s\n", Arrays.toString(amplifier.getBipolarRangesAvailable()));

                run_stream(amplifier.OpenImpedanceStream(), 0, 10, 1000);

                for (var sampling_rate : amplifier.getSamplingRatesAvailable()) {
                    run_stream(amplifier.OpenEegStream(sampling_rate), sampling_rate, 10, 250);
                }
            } finally {
                amplifier.close();
                amplifier = null;
            }
        }
    }

    private static void run_stream(com.eemagine.sdk.stream stream, int sampling_rate, int duration_s, int interval_ms) {
        var timer = new Timer();
        try {
            System.out.printf("stream:\n");
            System.out.printf("  duration.... %ds\n", duration_s);
            System.out.printf("  interval.... %dms\n", interval_ms);
            System.out.printf("  rate........ %d\n", sampling_rate);
            System.out.printf("  channels.... %s\n", Arrays.toString(stream.getChannelList()));

            var stream_poll = new StreamPoll(stream, sampling_rate > 0);
            timer.scheduleAtFixedRate(stream_poll, interval_ms, interval_ms);

            Thread.sleep(duration_s * 1000);
            System.out.printf("cancelling stream poll...\n");
            // stream_scheduled_future.cancel();
            System.out.printf("cancelled stream poll.\n");
        } catch (InterruptedException e) {
        } catch (Exception e) {
            System.out.printf("error in getting data: %s\n", e);
        } finally {
            System.out.printf("cancel timer...\n");
            timer.cancel();
            timer = null;
            System.out.printf("cancel timer done.\n");

            System.out.printf("closing stream...\n");
            stream.close();
            stream = null;
            System.out.printf("close stream done.\n");
        }
    }
}
