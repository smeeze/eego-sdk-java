if(DO_PYTHON)
  add_library(BoostPythonObjects OBJECT
    ${BOOST_SOURCES}/libs/python/src/converter/arg_to_python_base.cpp
    ${BOOST_SOURCES}/libs/python/src/converter/builtin_converters.cpp
    ${BOOST_SOURCES}/libs/python/src/converter/from_python.cpp
    ${BOOST_SOURCES}/libs/python/src/converter/registry.cpp
    ${BOOST_SOURCES}/libs/python/src/converter/type_id.cpp
    ${BOOST_SOURCES}/libs/python/src/dict.cpp
    ${BOOST_SOURCES}/libs/python/src/errors.cpp
#   ${BOOST_SOURCES}/libs/python/src/exec.cpp
#   ${BOOST_SOURCES}/libs/python/src/import.cpp
    ${BOOST_SOURCES}/libs/python/src/list.cpp
    ${BOOST_SOURCES}/libs/python/src/long.cpp
    ${BOOST_SOURCES}/libs/python/src/module.cpp
#   ${BOOST_SOURCES}/libs/python/src/numeric.cpp
#   ${BOOST_SOURCES}/libs/python/src/numpy/numpy.cpp
#   ${BOOST_SOURCES}/libs/python/src/numpy/dtype.cpp
#   ${BOOST_SOURCES}/libs/python/src/numpy/matrix.cpp
#   ${BOOST_SOURCES}/libs/python/src/numpy/ndarray.cpp
#   ${BOOST_SOURCES}/libs/python/src/numpy/numpy.cpp
#   ${BOOST_SOURCES}/libs/python/src/numpy/scalars.cpp
#   ${BOOST_SOURCES}/libs/python/src/numpy/ufunc.cpp
    ${BOOST_SOURCES}/libs/python/src/object/class.cpp
    ${BOOST_SOURCES}/libs/python/src/object/enum.cpp
    ${BOOST_SOURCES}/libs/python/src/object/function.cpp
    ${BOOST_SOURCES}/libs/python/src/object/function_doc_signature.cpp
    ${BOOST_SOURCES}/libs/python/src/object/inheritance.cpp
    ${BOOST_SOURCES}/libs/python/src/object/iterator.cpp
    ${BOOST_SOURCES}/libs/python/src/object/life_support.cpp
    ${BOOST_SOURCES}/libs/python/src/object_operators.cpp
    ${BOOST_SOURCES}/libs/python/src/object/pickle_support.cpp
    ${BOOST_SOURCES}/libs/python/src/object_protocol.cpp
    ${BOOST_SOURCES}/libs/python/src/object/stl_iterator.cpp
#   ${BOOST_SOURCES}/libs/python/src/slice.cpp
    ${BOOST_SOURCES}/libs/python/src/str.cpp
    ${BOOST_SOURCES}/libs/python/src/tuple.cpp
#   ${BOOST_SOURCES}/libs/python/src/wrapper.cpp
  )
  add_library(BoostPythonStatic          STATIC $<TARGET_OBJECTS:BoostPythonObjects>)
  add_library(BoostPython                SHARED $<TARGET_OBJECTS:BoostPythonObjects>)
  set_property(TARGET BoostPythonObjects PROPERTY COMPILE_FLAGS "-DBOOST_ALL_NO_LIB -DBOOST_PYTHON_SOURCE")

  target_include_directories(BoostPythonObjects SYSTEM PRIVATE ${BOOST_SOURCES})
  target_include_directories(BoostPythonObjects SYSTEM PRIVATE ${PYTHON_INCLUDE_DIRS})
  target_link_libraries(BoostPython ${PYTHON_LIBRARIES})
endif()
