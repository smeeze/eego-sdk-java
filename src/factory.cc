// fmt
#include <fmt/format.h>
// eego SDK
#include <eemagine/sdk/version.h>
#include <eemagine/sdk/wrapper.h>
// eego SDK JNI
#include <com_eemagine_sdk_factory.h>
// self
#include "helper.h"
///////////////////////////////////////////////////////////////////////////////
JNIEXPORT void JNICALL Java_com_eemagine_sdk_factory_init(JNIEnv *, jobject) {
  eemagine_sdk_init();
}
///////////////////////////////////////////////////////////////////////////////
JNIEXPORT void JNICALL Java_com_eemagine_sdk_factory_fini(JNIEnv *, jobject) {
  eemagine_sdk_exit();
}
///////////////////////////////////////////////////////////////////////////////
JNIEXPORT jstring JNICALL Java_com_eemagine_sdk_factory_getVersion(JNIEnv *env,
                                                                   jobject) {
  const auto s(fmt::format("{}.{}.{}.{}", EEGO_SDK_VERSION_MAJOR,
                           EEGO_SDK_VERSION_MINOR, EEGO_SDK_VERSION_MICRO,
                           EEGO_SDK_VERSION));
  return env->NewStringUTF(s.c_str());
}
///////////////////////////////////////////////////////////////////////////////
JNIEXPORT jintArray JNICALL
Java_com_eemagine_sdk_factory_get_1amplifier_1ids(JNIEnv *env, jobject) {
  static const int amplifier_info_array_count(64);
  eemagine_sdk_amplifier_info amplifier_info_array[amplifier_info_array_count];
  int amplifier_count(eemagine_sdk_get_amplifiers_info(
      amplifier_info_array, amplifier_info_array_count));

  eego_sdk_java::helper::validate_return_code(env, amplifier_count);

  jintArray rv(env->NewIntArray(amplifier_count));
  jint fill[amplifier_info_array_count];
  for (int i = 0; i < amplifier_count; ++i) {
    fill[i] = amplifier_info_array[i].id;
  }
  env->SetIntArrayRegion(rv, 0, amplifier_count, fill);
  return rv;
}
