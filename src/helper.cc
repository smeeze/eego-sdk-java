// sdk
#include <eemagine/sdk/wrapper.h>
// self
#include "helper.h"
///////////////////////////////////////////////////////////////////////////////
void eego_sdk_java::helper::validate_return_code(JNIEnv *env, int i) {
  if (i < 0) {
    switch (i) {
    case EEMAGINE_SDK_NOT_CONNECTED:
      env->ThrowNew(env->FindClass("java/lang/Exception"), "not connected");
      break;
    case EEMAGINE_SDK_ALREADY_EXISTS:
      env->ThrowNew(env->FindClass("java/lang/Exception"), "already exists");
      break;
    case EEMAGINE_SDK_NOT_FOUND:
      env->ThrowNew(env->FindClass("java/lang/Exception"), "not found");
      break;
    case EEMAGINE_SDK_INCORRECT_VALUE:
      env->ThrowNew(env->FindClass("java/lang/Exception"), "incorrect value");
      break;
    case EEMAGINE_SDK_INTERNAL_ERROR:
      env->ThrowNew(env->FindClass("java/lang/Exception"), "internal error");
      break;
    case EEMAGINE_SDK_UNKNOWN:
      env->ThrowNew(env->FindClass("java/lang/Exception"), "unknown");
      break;
    default:
      EEGO_SDK_JAVA_MARKER;
      break;
    }
  }
}
