#pragma once

// fmt
#include <fmt/format.h>
// jni
#include <jni.h>

#define EEGO_SDK_JAVA_MARKER                                                   \
  fmt::print("  marker.......... {} {}\n", __FILE__, __LINE__);

namespace eego_sdk_java {
namespace helper {
void validate_return_code(JNIEnv *, int);
}
} // namespace eego_sdk_java
