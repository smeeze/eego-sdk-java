// system
#include <vector>
// eego SDK
#include <eemagine/sdk/wrapper.h>
// eego SDK JNI
#include <com_eemagine_sdk_stream.h>
// self
#include "channel.h"
#include "helper.h"
///////////////////////////////////////////////////////////////////////////////
namespace {
int _get_stream_id(JNIEnv *env, jobject obj) {
  jclass clss(env->GetObjectClass(obj));
  jfieldID field(env->GetFieldID(clss, "_id", "I"));
  jint id(env->GetIntField(obj, field));
  return id;
}
} // namespace
///////////////////////////////////////////////////////////////////////////////
JNIEXPORT jobjectArray JNICALL
Java_com_eemagine_sdk_stream_getChannelList(JNIEnv *env, jobject obj) {
  const static int buf_size(1024);
  eemagine_sdk_channel_info channel_list[buf_size];

  const auto channel_count(eemagine_sdk_get_stream_channel_list(
      _get_stream_id(env, obj), channel_list, buf_size));
  eego_sdk_java::helper::validate_return_code(env, channel_count);

  jclass channel_class(env->FindClass("com/eemagine/sdk/channel"));
  jmethodID channel_class_init(env->GetMethodID(
      channel_class, "<init>", "(ILcom/eemagine/sdk/channel$channel_type;)V"));

  auto rv(env->NewObjectArray(channel_count, channel_class, NULL));
  for (int i = 0; i < channel_count; ++i) {
    auto obj(env->NewObject(
        channel_class, channel_class_init, channel_list[i].index,
        eego_sdk_java::channel::to_jni(env, channel_list[i].type)));
    env->SetObjectArrayElement(rv, i, obj);
    env->DeleteLocalRef(obj);
  }

  return rv;
}
///////////////////////////////////////////////////////////////////////////////
JNIEXPORT jobject JNICALL Java_com_eemagine_sdk_stream_getData(JNIEnv *env,
                                                               jobject obj) {
  const auto id(_get_stream_id(env, obj));
  eego_sdk_java::helper::validate_return_code(env, id);

  const auto channel_count(eemagine_sdk_get_stream_channel_count(id));
  eego_sdk_java::helper::validate_return_code(env, channel_count);

  const int bytes_to_allocate(eemagine_sdk_prefetch(id));
  eego_sdk_java::helper::validate_return_code(env, bytes_to_allocate);

  int double_count(bytes_to_allocate / sizeof(double));
  int sample_count(double_count / channel_count);

  std::vector<double> buffer(double_count);
  eego_sdk_java::helper::validate_return_code(
      env, eemagine_sdk_get_data(id, buffer.data(), bytes_to_allocate));

  jdoubleArray java_double_array(env->NewDoubleArray(double_count));
  env->SetDoubleArrayRegion(java_double_array, 0, double_count, buffer.data());

  jclass buffer_class(env->FindClass("com/eemagine/sdk/buffer"));
  jmethodID buffer_class_init(
      env->GetMethodID(buffer_class, "<init>", "(II[D)V"));

  auto rv(env->NewObject(buffer_class, buffer_class_init, channel_count,
                         sample_count, java_double_array));
  // env->DeleteLocalRef(rv);

  return rv;
}
///////////////////////////////////////////////////////////////////////////////
JNIEXPORT void JNICALL Java_com_eemagine_sdk_stream__1close(JNIEnv *env,
                                                            jobject obj) {
  const auto id(eemagine_sdk_close_stream(_get_stream_id(env, obj)));
  fmt::print("--- {}, id={} ---\n", __PRETTY_FUNCTION__, id);
  eego_sdk_java::helper::validate_return_code(env, id);
}
