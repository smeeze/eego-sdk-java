// system
#include <type_traits>
// jni
#include <jni.h>
///////////////////////////////////////////////////////////////////////////////
namespace {
static_assert(std::is_same<double, jdouble>::value,
              "double and jdouble need to be the same type");
} // namespace
