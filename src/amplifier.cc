// eego SDK
#include <eemagine/sdk/wrapper.h>
// eego SDK JNI
#include <com_eemagine_sdk_amplifier.h>
// self
#include "channel.h"
#include "helper.h"
///////////////////////////////////////////////////////////////////////////////
namespace {
int _get_amplifier_id(JNIEnv *env, jobject obj) {
  jclass clss(env->GetObjectClass(obj));
  jfieldID field(env->GetFieldID(clss, "_id", "I"));
  jint id(env->GetIntField(obj, field));
  return id;
}
} // namespace
///////////////////////////////////////////////////////////////////////////////
JNIEXPORT jstring JNICALL Java_com_eemagine_sdk_amplifier_getType(JNIEnv *env,
                                                                  jobject obj) {
  const static int buf_size(1024);
  char buf[buf_size];
  eego_sdk_java::helper::validate_return_code(
      env, eemagine_sdk_get_amplifier_type(_get_amplifier_id(env, obj), buf,
                                           buf_size));
  std::string s(buf);
  return env->NewStringUTF(s.c_str());
}
///////////////////////////////////////////////////////////////////////////////
JNIEXPORT jint JNICALL
Java_com_eemagine_sdk_amplifier_getFirmwareVersion(JNIEnv *env, jobject obj) {
  const int firmware_version(
      eemagine_sdk_get_amplifier_version(_get_amplifier_id(env, obj)));
  eego_sdk_java::helper::validate_return_code(env, firmware_version);
  return firmware_version;
}
///////////////////////////////////////////////////////////////////////////////
JNIEXPORT jobjectArray JNICALL
Java_com_eemagine_sdk_amplifier_getChannelList(JNIEnv *env, jobject obj) {
  const static int buf_size(1024);
  eemagine_sdk_channel_info channel_list[buf_size];

  const auto channel_count(eemagine_sdk_get_amplifier_channel_list(
      _get_amplifier_id(env, obj), channel_list, buf_size));
  eego_sdk_java::helper::validate_return_code(env, channel_count);

  jclass channel_class(env->FindClass("com/eemagine/sdk/channel"));
  jmethodID channel_class_init(
      // env->GetMethodID(channel_class, "<init>", "(I)V"));
      env->GetMethodID(channel_class, "<init>",
                       "(ILcom/eemagine/sdk/channel$channel_type;)V"));

  auto rv(env->NewObjectArray(channel_count, channel_class, NULL));
  for (int i = 0; i < channel_count; ++i) {
    auto obj(env->NewObject(
        channel_class, channel_class_init, channel_list[i].index,
        eego_sdk_java::channel::to_jni(env, channel_list[i].type)));
    env->SetObjectArrayElement(rv, i, obj);
    env->DeleteLocalRef(obj);
  }

  return rv;
}
///////////////////////////////////////////////////////////////////////////////
JNIEXPORT jintArray JNICALL
Java_com_eemagine_sdk_amplifier_getSamplingRatesAvailable(JNIEnv *env,
                                                          jobject obj) {
  static const int sampling_rates_available_array_count(64);
  int sampling_rates_available_array[sampling_rates_available_array_count];
  int sampling_rates_count(eemagine_sdk_get_amplifier_sampling_rates_available(
      _get_amplifier_id(env, obj), sampling_rates_available_array,
      sampling_rates_available_array_count));

  eego_sdk_java::helper::validate_return_code(env, sampling_rates_count);

  jintArray rv(env->NewIntArray(sampling_rates_count));
  jint fill[sampling_rates_available_array_count];
  std::copy(std::begin(sampling_rates_available_array),
            std::end(sampling_rates_available_array), std::begin(fill));
  env->SetIntArrayRegion(rv, 0, sampling_rates_count, fill);
  return rv;
}
///////////////////////////////////////////////////////////////////////////////
JNIEXPORT jdoubleArray JNICALL
Java_com_eemagine_sdk_amplifier_getReferenceRangesAvailable(JNIEnv *env,
                                                            jobject obj) {
  static const int reference_ranges_available_array_count(64);
  double
      reference_ranges_available_array[reference_ranges_available_array_count];
  int reference_ranges_count(
      eemagine_sdk_get_amplifier_reference_ranges_available(
          _get_amplifier_id(env, obj), reference_ranges_available_array,
          reference_ranges_available_array_count));

  eego_sdk_java::helper::validate_return_code(env, reference_ranges_count);

  jdoubleArray rv(env->NewDoubleArray(reference_ranges_count));
  env->SetDoubleArrayRegion(rv, 0, reference_ranges_count,
                            reference_ranges_available_array);
  return rv;
}
///////////////////////////////////////////////////////////////////////////////
JNIEXPORT jdoubleArray JNICALL
Java_com_eemagine_sdk_amplifier_getBipolarRangesAvailable(JNIEnv *env,
                                                          jobject obj) {
  static const int bipolar_ranges_available_array_count(64);
  double bipolar_ranges_available_array[bipolar_ranges_available_array_count];
  int bipolar_ranges_count(eemagine_sdk_get_amplifier_bipolar_ranges_available(
      _get_amplifier_id(env, obj), bipolar_ranges_available_array,
      bipolar_ranges_available_array_count));

  eego_sdk_java::helper::validate_return_code(env, bipolar_ranges_count);

  jdoubleArray rv(env->NewDoubleArray(bipolar_ranges_count));
  env->SetDoubleArrayRegion(rv, 0, bipolar_ranges_count,
                            bipolar_ranges_available_array);
  return rv;
}
///////////////////////////////////////////////////////////////////////////////
JNIEXPORT jstring JNICALL
Java_com_eemagine_sdk_amplifier_getSerialNumber(JNIEnv *env, jobject obj) {
  const static int buf_size(1024);
  char buf[buf_size];
  eego_sdk_java::helper::validate_return_code(
      env, eemagine_sdk_get_amplifier_serial(_get_amplifier_id(env, obj), buf,
                                             buf_size));
  std::string s(buf);
  return env->NewStringUTF(s.c_str());
}
///////////////////////////////////////////////////////////////////////////////
JNIEXPORT jint JNICALL Java_com_eemagine_sdk_amplifier_open_1eeg_1stream(
    JNIEnv *env, jobject obj, jint sampling_rate, jdouble ref_range,
    jdouble bip_range) {
  const auto id(_get_amplifier_id(env, obj));
  eego_sdk_java::helper::validate_return_code(env, id);

  jint rv(eemagine_sdk_open_eeg_stream(id, sampling_rate, ref_range, bip_range,
                                       (unsigned long long)(-1),
                                       (unsigned long long)(-1)));

  eego_sdk_java::helper::validate_return_code(env, rv);
  return rv;
}
///////////////////////////////////////////////////////////////////////////////
JNIEXPORT jint JNICALL Java_com_eemagine_sdk_amplifier_open_1impedance_1stream(
    JNIEnv *env, jobject obj) {
  const auto id(_get_amplifier_id(env, obj));
  eego_sdk_java::helper::validate_return_code(env, id);

  jint rv(eemagine_sdk_open_impedance_stream(id, (unsigned long long)(-1)));

  eego_sdk_java::helper::validate_return_code(env, rv);
  return rv;
}
///////////////////////////////////////////////////////////////////////////////
JNIEXPORT void JNICALL Java_com_eemagine_sdk_amplifier__1open(JNIEnv *env,
                                                              jobject obj) {
  const auto id(_get_amplifier_id(env, obj));
  eego_sdk_java::helper::validate_return_code(env,
                                              eemagine_sdk_open_amplifier(id));
}
///////////////////////////////////////////////////////////////////////////////
JNIEXPORT void JNICALL Java_com_eemagine_sdk_amplifier__1close(JNIEnv *env,
                                                               jobject obj) {

  const auto id(_get_amplifier_id(env, obj));
  eego_sdk_java::helper::validate_return_code(env,
                                              eemagine_sdk_close_amplifier(id));
}
