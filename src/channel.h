#pragma once

// JNI
#include <jni.h>
// eego SDK
#include <eemagine/sdk/wrapper.h>

namespace eego_sdk_java {
class channel {
public:
  static jobject to_jni(JNIEnv *, const eemagine_sdk_channel_type &);
};
} // namespace eego_sdk_java
