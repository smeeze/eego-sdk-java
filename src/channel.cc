// self
#include "channel.h"
#include "helper.h"
///////////////////////////////////////////////////////////////////////////////
namespace {
jobject _to_jni(JNIEnv *env, const char *label) {
  jclass channel_type_class(
      env->FindClass("com/eemagine/sdk/channel$channel_type"));
  jfieldID channel_type_field_id(env->GetStaticFieldID(
      channel_type_class, label, "Lcom/eemagine/sdk/channel$channel_type;"));
  return env->GetStaticObjectField(channel_type_class, channel_type_field_id);
}
} // namespace
///////////////////////////////////////////////////////////////////////////////
jobject eego_sdk_java::channel::to_jni(JNIEnv *env,
                                       const eemagine_sdk_channel_type &ct) {
  switch (ct) {
  case EEMAGINE_SDK_CHANNEL_TYPE_REFERENCE:
    return _to_jni(env, "reference");
    break;
  case EEMAGINE_SDK_CHANNEL_TYPE_BIPOLAR:
    return _to_jni(env, "bipolar");
    break;
  case EEMAGINE_SDK_CHANNEL_TYPE_ACCELEROMETER:
    return _to_jni(env, "accelerometer");
    break;
  case EEMAGINE_SDK_CHANNEL_TYPE_GYROSCOPE:
    return _to_jni(env, "gyroscopy");
    break;
  case EEMAGINE_SDK_CHANNEL_TYPE_MAGNETOMETER:
    return _to_jni(env, "magnetometer");
    break;
  case EEMAGINE_SDK_CHANNEL_TYPE_TRIGGER:
    return _to_jni(env, "trigger");
    break;
  case EEMAGINE_SDK_CHANNEL_TYPE_SAMPLE_COUNTER:
    return _to_jni(env, "sample_counter");
    break;
  case EEMAGINE_SDK_CHANNEL_TYPE_IMPEDANCE_REFERENCE:
    return _to_jni(env, "impedance_reference");
    break;
  case EEMAGINE_SDK_CHANNEL_TYPE_IMPEDANCE_GROUND:
    return _to_jni(env, "impedance_ground");
    break;
  }

  return _to_jni(env, "none");
}
