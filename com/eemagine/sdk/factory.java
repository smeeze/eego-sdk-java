package com.eemagine.sdk;

import com.eemagine.sdk.amplifier;

public class factory {
    static {
        String name = "Linux";
        if (System.getProperty("os.name").toLowerCase().indexOf("win") >= 0) {
            name = "Windows";
        }
        String arch = "32";
        if (System.getProperty("os.arch").toLowerCase().indexOf("64") >= 0) {
            arch = "64";
        }
        System.loadLibrary("eego-SDK-JNI-" + name + "-" + arch);
    }

    //
    //
    //
    public factory() {
        init();
    }

    public native String getVersion();

    public amplifier getAmplifier() {
        return getAmplifiers()[0];
    }

    public amplifier[] getAmplifiers() {
        int[] amplifier_ids = get_amplifier_ids();
        amplifier[] rv = new amplifier[amplifier_ids.length];

        for (int i = 0; i < amplifier_ids.length; ++i) {
            rv[i] = new amplifier(amplifier_ids[i]);
        }

        return rv;
    }

    //
    //
    //
    private native void init();

    private native int[] get_amplifier_ids();
}
