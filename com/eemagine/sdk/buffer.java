package com.eemagine.sdk;

public final class buffer {
    //
    //
    //
    public buffer(int channel_count, int sample_count, double[] data) {
        _channel_count = channel_count;
        _sample_count = sample_count;
        _data = data;
    }

    public int getChannelCount() {
        return _channel_count;
    }

    public int getSampleCount() {
        return _sample_count;
    }

    public double[] getData() {
        return _data;
    }

    public double getSample(int channel, int sample) {
        if (channel >= _channel_count || sample >= _sample_count) {
            throw new IllegalArgumentException("getSample index");
        }
        return _data[channel + sample * _channel_count];
    }

    //
    //
    //
    @Override
    public String toString() {
        return String.format("buffer(%d x %d)", getChannelCount(), getSampleCount());
    }

    //
    //
    //
    private int _channel_count;
    private int _sample_count;
    private double[] _data;
}
