package com.eemagine.sdk;

public final class channel {
    enum channel_type {
        none, reference, bipolar, trigger, sample_counter, impedance_reference, impedance_ground, accelerometer,
        gyroscope, magnetometer
    };

    //
    //
    //
    public channel(int index, channel_type ct) {
        _index = index;
        _channel_type = ct;
    }

    public int getIndex() {
        return _index;
    }

    public channel_type getChannelType() {
        return _channel_type;
    }

    //
    //
    //
    @Override
    public String toString() {
        return String.format("chan(%d, %s)", getIndex(), getChannelType());
    }

    //
    //
    //
    private int _index;
    private channel_type _channel_type;
}
