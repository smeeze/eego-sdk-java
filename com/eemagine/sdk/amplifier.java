package com.eemagine.sdk;

import com.eemagine.sdk.channel;
import com.eemagine.sdk.stream;

public class amplifier {
    public amplifier(int id) {
        _id = id;
        _open();
    }

    public void close() {
        _close();
    }

    public native String getType();

    public native int getFirmwareVersion();

    public native String getSerialNumber();

    public native channel[] getChannelList();

    public native int[] getSamplingRatesAvailable();

    public native double[] getReferenceRangesAvailable();

    public native double[] getBipolarRangesAvailable();

    public stream OpenEegStream(int sampling_rate, double ref_range, double bip_range) {
        return new stream(open_eeg_stream(sampling_rate, ref_range, bip_range));
    }

    public stream OpenEegStream(int sampling_rate) {
        return OpenEegStream(sampling_rate, getReferenceRangesAvailable()[0], getBipolarRangesAvailable()[0]);
    }

    public stream OpenImpedanceStream() {
        return new stream(open_impedance_stream());
    }

    //
    //
    //
    private int _id;

    private native int open_eeg_stream(int sampling_rate, double ref_range, double bip_range);

    private native int open_impedance_stream();

    private native void _open();

    private native void _close();
}
