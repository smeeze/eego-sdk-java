package com.eemagine.sdk;

import com.eemagine.sdk.channel;
import com.eemagine.sdk.buffer;

public class stream {
    public stream(int id) {
        _id = id;
    }

    public void close() {
        _close();
    }

    public native channel[] getChannelList();

    public native buffer getData();

    //
    //
    //
    private int _id;

    private native void _close();
}
