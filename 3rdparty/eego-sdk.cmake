message("using EEGO_SDK_ZIP: ${EEGO_SDK_ZIP}")
include(FetchContent)
FetchContent_Declare(
  eego_sdk
  URL ${EEGO_SDK_ZIP}
)

FetchContent_GetProperties(eego_sdk)
if(NOT eego_sdk_POPULATED)
  MESSAGE("need to populate eego-SDK...")
  FetchContent_Populate(eego_sdk)

  if(ANDROID)
    find_library(EEGO_SDK_LIBRARY NAMES eego-SDK PATHS ${eego_sdk_SOURCE_DIR}/android/${ANDROID_ABI} REQUIRED)
  endif()

  if(${CMAKE_SYSTEM_NAME} MATCHES Linux)
    if(CMAKE_SIZEOF_VOID_P EQUAL 4)
      find_library(EEGO_SDK_LIBRARY NAMES eego-SDK PATHS ${eego_sdk_SOURCE_DIR}/linux/32bit REQUIRED)
    else()
      find_library(EEGO_SDK_LIBRARY NAMES eego-SDK PATHS ${eego_sdk_SOURCE_DIR}/linux/64bit REQUIRED)
    endif()
  endif()

  if(${CMAKE_SYSTEM_NAME} MATCHES Windows)
    if(CMAKE_SIZEOF_VOID_P EQUAL 4)
      find_library(EEGO_SDK_LIBRARY NAMES eego-SDK PATHS ${eego_sdk_SOURCE_DIR}/windows/32bit REQUIRED)
    else()
      find_library(EEGO_SDK_LIBRARY NAMES eego-SDK PATHS ${eego_sdk_SOURCE_DIR}/windows/64bit REQUIRED)
    endif()
  endif()

  set(EEGO_SDK_ROOT ${eego_sdk_SOURCE_DIR} PARENT_SCOPE)
  set(EEGO_SDK_GUI_HAVE_EEGO TRUE PARENT_SCOPE)
  MESSAGE("done populating eego-SDK")
endif()
